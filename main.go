package main

import (
	"fmt"
	"github.com/stellar/go/build"
	"github.com/stellar/go/clients/horizon"
	"math/rand"
	"net/http"
	"os"
	"strconv"
)

func main() {
	horizonUrl := os.Args[1]
	sourceAccount := os.Args[2]
	pk := os.Args[3]
	amount := rand.Intn(100)
	amountStr := strconv.FormatInt(int64(amount), 10)
	destinationAccount := os.Args[4]

	fmt.Printf("source account: %s\n", sourceAccount)
	fmt.Printf("dumping stellar transaction to %s\n", horizonUrl)
	fmt.Printf("sending %d XLM to %s\n", amount, destinationAccount)

	horizonClient := horizon.Client{
		URL: horizonUrl,
		HTTP: http.DefaultClient,
	}

	acct, err := horizonClient.LoadAccount(sourceAccount)
	if err != nil {
		panic(err)
	}
	seqNum, err := strconv.ParseInt(acct.Sequence, 10, 64)
	if err != nil {
		panic(err)
	}
	unsignedSeqNum := uint64(seqNum) + 1

	tx, err := build.Transaction(
		build.TestNetwork,
		build.SourceAccount{sourceAccount},
		build.Sequence{unsignedSeqNum},
		build.Payment(
			build.Destination{destinationAccount},
			build.NativeAmount{amountStr},
		),
	)
	if err != nil {
		panic(err)
	}

	txe, err := tx.Sign(pk)
	if err != nil {
		panic(err)
	}

	txeB64, err := txe.Base64()
	if err != nil {
		panic(err)
	}

	resp, err := horizonClient.SubmitTransaction(txeB64)
	if err != nil {
		panic(err)
	}

	fmt.Println("successful transaction")
	fmt.Println("ledger: ", resp.Ledger)
	fmt.Println("hash: ", resp.Hash)
}
